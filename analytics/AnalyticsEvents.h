
#pragma once

#ifndef PRAV_PUBLIC_API

#if ( defined( __unix__ ) || defined( __APPLE__ ) ) && ( defined( __GNUC__ ) || defined( __clang__ ) )
#define PRAV_PUBLIC_API    __attribute__( ( __visibility__ ( "default" ) ) )
#elif defined( WIN32 ) && defined( BUILDING_DLL )
#define PRAV_PUBLIC_API    __declspec ( dllexport )
#elif defined( WIN32 )
#define PRAV_PUBLIC_API    __declspec ( dllimport )
#else
#define PRAV_PUBLIC_API
#endif
#endif

#ifdef __cplusplus
extern "C" {
#endif

/// @brief Enum for the different SIP Events to record during a call
/// (see Analytics::SIPCallEvent::CallStateEvent)
enum SipCallEvent
{
    SIP_CALL_EVENT_UNKNOWN = 0,                     ///< Unknown event
    SIP_CALL_EVENT_INVITE = 1,                      ///< SIP call invite event
    SIP_CALL_EVENT_RINGING = 2,                     ///< SIP call ringing event
    SIP_CALL_EVENT_ANSWERED = 3,                    ///< SIP call answered event
    SIP_CALL_EVENT_BYE = 4                          ///< SIP call bye event
};

/// @brief Enum for the different SIP message directions
/// (see Analytics::SIPCallEvent::Direction)
enum SipMsgDirection
{
    SIP_MSG_DIRECTION_UNKNOWN = 0,                     ///< Unknown SIP message direction
    SIP_MSG_DIRECTION_SENT = 1,                        ///< Sent SIP message
    SIP_MSG_DIRECTION_RECEIVED = 2                     ///< Received SIP message
};

/// @brief Enum for the different virtual socket types
/// (see Analytics::VirtualSocketType)
enum VSocketType
{
    VSOCK_TYPE_UNKNOWN = 0,                             ///< Unknown socket
    VSOCK_TYPE_SEAMLESS = 1,                            ///< Seamless socket
    VSOCK_TYPE_HIGH_PRIORITY_SEAMLESS = 2,              ///< High priority seamless socket
    VSOCK_TYPE_REPLICATION = 3,                         ///< Replication socket
    VSOCK_TYPE_HIGH_PRIORITY_REPLICATION = 4,           ///< High priority replication socket
    VSOCK_TYPE_BANDWIDTH_AGGREGATION = 5,               ///< Flow-based bandwidth aggregation socket
    VSOCK_TYPE_PREFER_WIFI = 6,                         ///< Prefer WiFi socket
    VSOCK_TYPE_PREFER_MOBILE = 7,                       ///< Prefer mobile socket
    VSOCK_TYPE_THRESHOLD_SEAMLESS = 8,                  ///< Threshold seamless socket
    VSOCK_TYPE_SINGLE_STREAM_AGGREGATION = 9,           ///< Single stream aggregation socket
    VSOCK_TYPE_WIFI_ONLY = 10,                          ///< WiFi only socket
    VSOCK_TYPE_MOBILE_ONLY = 11,                        ///< Mobile only socket
    VSOCK_TYPE_BANDWIDTH_AGGREGATION_BALANCED = 12      ///< Flow-based bandwidth aggregation socket with balancing
};

/// @brief Writes a system event to be uploaded to the analytics server
/// @param [in] customerAssignedId The customer assigned ID to set; or NULL if not present
/// @param [in] productName The product name to set; or NULL if not present
/// @param [in] productVersion The product version to set; or NULL if not present
/// @return One of ANALYTICS_RET_CODE_* values (see Analytics.h), where values < 0 are an error, 0 is success, and > 0
///         indicates a non-error condition with additional information.
PRAV_PUBLIC_API int analyticsWriteSystemEvent (
        const char * customerAssignedId, const char * productName, const char * productVersion );

/// @brief Writes a SIP call event to be uploaded to the analytics server
/// @param [in] sockFd The socket file descriptor, or < 0 if not present.
/// @param [in] event The type of event to report
/// @param [in] direction The direction of the SIP message used to determine the call event
/// @param [in] callId A string used to identify the call; or NULL if not present
/// @param [in] callFrom A string used to identify the caller; or NULL if not present
/// @param [in] callTo A string used to identify the callee; or NULL if not present
/// @return One of ANALYTICS_RET_CODE_* values (see Analytics.h), where values < 0 are an error, 0 is success, and > 0
///         indicates a non-error condition with additional information.
PRAV_PUBLIC_API int analyticsWriteSIPCallEvent (
        int sockFd, enum SipCallEvent event, enum SipMsgDirection direction,
        const char * callId, const char * callFrom, const char * callTo );

/// @brief Writes a socket tag map event to be uploaded to the analytics server
/// @param [in] tags The array of socket tags; must not be NULL
/// @param [in] types The array of virtual socket types, must not be NULL
/// @param [in] numItems The number of tag:type pairs; must be > 0.
///                      The length of the tags and types arrays must be equal to numItems
/// @return One of ANALYTICS_RET_CODE_* values (see Analytics.h), where values < 0 are an error, 0 is success, and > 0
///         indicates a non-error condition with additional information.
PRAV_PUBLIC_API int analyticsWriteSocketTagMapEvent (
        const int * tags, const enum VSocketType * types, unsigned int numItems );

#ifdef __cplusplus
}
#endif
