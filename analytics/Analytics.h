
#pragma once

/// @brief Indicates that the maximum number of events per upload were uploaded
#define ANALYTICS_RET_CODE_MAX_NEW_EVENTS    2

/// @brief Indicates there were no new events to upload (not an error)
#define ANALYTICS_RET_CODE_NO_NEW_EVENTS     1

/// @brief Indicates that the operation was successful
#define ANALYTICS_RET_CODE_OK                0

/// @brief Indicates that there was an internal error
#define ANALYTICS_RET_CODE_INTERNAL          -1

/// @brief Indicates that there was an error related to the filesystem
#define ANALYTICS_RET_CODE_FILESYSTEM        -2

/// @brief Indicates that there was a network error
#define ANALYTICS_RET_CODE_NETWORK           -3

/// @brief Indicates that there was a timeout uploading the event
#define ANALYTICS_RET_CODE_TIMEOUT           -4

/// @brief Indicates that there was an unknown error
#define ANALYTICS_RET_CODE_UNKNOWN           -5

/// @brief Indicates that there was an error writing an event to a file
#define ANALYTICS_RET_CODE_WRITE_FAILURE     -6

/// @brief Indicates that there was a proto error (missing required field, serialization/deserialization problem, etc)
#define ANALYTICS_RET_CODE_PROTOCOL_ERROR    -7

#ifndef PRAV_PUBLIC_API

#if ( defined( __unix__ ) || defined( __APPLE__ ) ) && ( defined( __GNUC__ ) || defined( __clang__ ) )
#define PRAV_PUBLIC_API    __attribute__( ( __visibility__ ( "default" ) ) )
#elif defined( WIN32 ) && defined( BUILDING_DLL )
#define PRAV_PUBLIC_API    __declspec ( dllexport )
#elif defined( WIN32 )
#define PRAV_PUBLIC_API    __declspec ( dllimport )
#else
#define PRAV_PUBLIC_API
#endif
#endif

#ifdef __cplusplus
extern "C" {
#endif

/// @brief Initialize the analytics library.
/// This function must be called before any other threads start.
/// It must not be called from DLLMain or a static initializer in a Windows DLL.
/// @return 1 if initialization has succeeded; 0 otherwise.
PRAV_PUBLIC_API int analyticsInit();

/// @brief Configures the analytics server URL
/// @param [in] url The URL on the analytics server to upload events to
/// @param [in] maxEventsPerUpload Maximum number events to batch together in each upload, 0 or less for no limit
/// @return One of ANALYTICS_RET_CODE_* values (see above), where values < 0 are an error, 0 is success, and > 0
///         indicates a non-error condition with additional information.
PRAV_PUBLIC_API int analyticsConfigureServer ( const char * url, int maxEventsPerUpload );

/// @brief Configures properties needed to write analytics events
/// @param [in] eventPath The directory where the event files will be written
/// @param [in] customerId The unique ID assigned (by Carnegie) to the customer
/// @param [in] installId The unique ID assigned (by Carnegie) to the product
/// @return One of ANALYTICS_RET_CODE_* values (see above), where values < 0 are an error, 0 is success, and > 0
///         indicates a non-error condition with additional information.
PRAV_PUBLIC_API int analyticsConfigureEvents (
        const char * eventPath, const char * customerId, const char * installId );

/// @brief Uploads analytics events to the server
/// @param [in] timeout The amount of time allowed (in seconds) to upload events to the server
///             If timeout <= 0: blocks until error occurs, or all events are successfully sent
///             If timeout > 0: blocks with a maximum timeout
/// @return One of ANALYTICS_RET_CODE_* values (see above), where values < 0 are an error, 0 is successful and all
///         events were uploaded, and > 0 indicates a non-error condition with additional information.
///         ANALYTICS_RET_CODE_NO_NEW_EVENTS indicates that there were no new analytics events to upload, which is
///         normal when no new events have been generated.
PRAV_PUBLIC_API int analyticsUploadEvents ( int timeout );

#ifdef __cplusplus
}
#endif
